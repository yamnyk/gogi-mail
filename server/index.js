const express = require('express');
const {urlencoded, json} = require('express');
const jwt = require('jsonwebtoken');
const path = require('path');
const {getToken} = require("./authHelpers.js");
const config = require("./config.js");
const {v4} = require("uuid");

const app = express();

app.use(json());
app.use(urlencoded({extended: false}));
app.use(express.static(path.resolve(path.dirname("")) + "/public/"));

const port = process.env.PORT || 8085;

const USERS = [{name: 'Gogi'}];

const EMAILS = {
  "inbox": [
    {
      "id": "0001",
      "subject": "Delivery order #32232323",
      "from": "delivery@eshop.coco.com",
      "text": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quos ullam. Cumque dolor eaque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quoolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quos ullam. Cumque dolor eaque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!"
    },
    {
      "id": "0002",
      "subject": "You're fired",
      "from": "boss@bossy.boss.com",
      "text": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quos ullam. Cumque dolor eaque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quos uehenderit sint ut!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quos ullam. Cumque dolor eaque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!"
    },
    {
      "id": "0003",
      "subject": "Your car is ready!",
      "from": "legasy@service.com",
      "text": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequunturt repellendus reprehenderit sint ut!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quos ullam. Cumque dolor eaque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic ius dolor eaque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!"
    },
    {
      "id": "0004",
      "subject": "Wat do you think about our new features",
      "from": "body-parser@npm.no",
      "text": "Lorem ipsum dolor sit amet, consque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quos ullam. Cumque dolor eaque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quos ullam. Cumque dolor eaque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!"
    },
    {
      "id": "0005",
      "subject": "TOP SALE!! 90% OFF",
      "from": "sales@kexshop.nd",
      "text": "Lorem ipsum dolor sadipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quos ullam. Cumque dolor eaque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!"
    },
    {
      "id": "0006",
      "subject": "Ya tibya po IP vichislil!!!",
      "from": "unknown",
      "text": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quos ullam. Cumque dolor eaque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quos ullam. Cumque dolor eaque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quos ullam. Cumque dolor eaque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!"
    }
  ],
  "sent": [
    {
      "id": "0007",
      "subject": "Give me back my money",
      "to": "sales@kexshop.nd",
      "text": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quos ullam. Cumque dolor eaque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quos ullam. Cumque dolor eaque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quos ullam. Cumque dolor eaque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!"
    },
    {
      "id": "0008",
      "subject": "Contract #588820 closed",
      "to": "big-boss@bossy.boss.com",
      "text": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quos ullam. Cumque dolor eaque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quos ullam. Cumque dolor eaque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur fuga hic iusto nesciunt nostrum porro quos ullam. Cumque dolor eaque eveniet pariatur quam qui repellat repellendus reprehenderit sint ut!"
    }
  ],
  "draft": [
    {
      "id": "0009",
      "subject": "",
      "to": "hr@terrasoft",
      "text": "I'm looking for a job...."
    },
    {
      "id": "00010",
      "subject": "About Tuesday meeting",
      "to": "nobody@from-the.wc",
      "text": ""
    },
    {
      "id": "00011",
      "subject": "",
      "to": "",
      "text": "What did yo mean, saying "
    }
  ],
  "spam": []

};

const FAVOURITES = [];

const freeOfAuth = ["/api/auth", "/api/auth/register", "/public"];

app.use('*', (req, res, next) => {
  if (!freeOfAuth.some((url) => url === req.baseUrl)) {
    if (req.headers["authorization"]) {
      jwt.verify(
        req.headers["authorization"],
        config.SECRET_KEY,
        {},
        async (err, decoded) => {
          if (err) {
            res.status(403);
            res.send({message: "permission denied"});
          }
          const {name} = decoded;
          const userFromDB = USERS.find(u => u.name === name);

          if (userFromDB) {
            next();
          } else {
            res.status(403);
            res.send({message: "permission denied"});
          }
        }
      );
    } else {
      res.status(403);
      res.send({message: "permission denied"});
    }
  } else {
    next();
  }
})

app.get('/api/emails', (req, res) => {
  res.send(EMAILS)
})

app.post('/api/emails/:folderName', (req, res) => {
  const {folderName} = req.params;
  const newEmail = {id: v4(), ...req.body};

  if (!EMAILS[folderName]) {
    res.status(404).send({message: 'folder not found'})
    return
  }

  EMAILS[folderName].push(newEmail);
  res.send(newEmail)
})

app.get('/api/favs', (req, res) => {
  res.send(FAVOURITES)
})

app.get('/api/emails/:emailId', (req, res) => {
  const {emailId} = req.params;
  const allEmails = Object.values(EMAILS).flat(1);

  res.send(allEmails.find(e => e.id === emailId))
})

app.get('/api/emails/inbox', (req, res) => {
  res.send(EMAILS.inbox)
})
app.get('/api/emails/sent', (req, res) => {
  res.send(EMAILS.sent)
})
app.get('/api/emails/draft', (req, res) => {
  res.send(EMAILS.draft)
})
app.get('/api/emails/spam', (req, res) => {
  res.send(EMAILS.spam)
})

app.get('/api/favs/:emailId', (req, res) => {
  const {emailId} = req.params;

  res.send(FAVOURITES.find(e => e.id === emailId))
})

app.post('/api/favs', (req, res) => {
  const {id} = req.body;
  const allEmails = Object.values(EMAILS).flat(1);
  const isEmailExists = allEmails.find(e => e.id === id);

  if (isEmailExists) {
    FAVOURITES.push(isEmailExists)
    res.send(isEmailExists)
  } else {
    res.status(404).send({message: 'No such email'})
  }
});

app.delete("/api/favs/:id", (req, res) => {
  const id = req.params.id
  const indexOfEmail = FAVOURITES.findIndex((email) => email.id === id);
  const updatedList = FAVOURITES.splice(indexOfEmail,1);
  res.send(updatedList)
})

app.post('/api/auth', (req, res) => {
  const {name} = req.body;

  const isValidUser = USERS.find(u => u.name === name);

  if (isValidUser) {
    res.send({message: 'User authenticated', token: getToken(isValidUser)})
  } else {
    res.status(401).send({message: 'No such user'})
  }
})

app.post('/api/auth/register', (req, res) => {
  const {name} = req.body;
  const isAlreadyRegistered = USERS.find(u => u.name === name);

  if (!isAlreadyRegistered) {
    const newUser = {name};

    USERS.push(newUser)
    res.send({data: newUser})
  } else {
    res.status(409).send({message: "User already exists"})
  }
})

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/public/index.html')
})

app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})

app.get('*', (req, res) => {
  res.sendFile(__dirname + '/public/index.html')
})