const jwt = require("jsonwebtoken");
const config = require("./config.js");

const getToken = (userFromDB) => {
  const payload = {
    name: userFromDB.name,
  };
  return jwt.sign(payload, config.SECRET_KEY, {
    expiresIn: "2 days",
    // expiresIn: 60
  });
};

const decode = (auth) => {
  return jwt.verify(auth, config.SECRET_KEY);
};

const authHelpers = {
  decode,
  getToken,
};

module.exports = authHelpers;