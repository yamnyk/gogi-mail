import React from "react";
import {render, screen} from '@testing-library/react';
import EmailCard from "./EmailCard";

const card = {
  from: 'from',
  to: 'to',
  subject: 'subject',
  text: 'text',
};

describe('EmailCard tests', () => {
  test('EmailsCard smoke test', () => {
    render(<EmailCard data={card} setActive={gg} isActive/>);
  });

  test("card text isn't shown when isActive", ()=> {
    const {queryByText, container} = render(<EmailCard data={card} />);
    const cText = queryByText(container, card.text);
    expect(cText).not.toBeInTheDocument();
  });

});