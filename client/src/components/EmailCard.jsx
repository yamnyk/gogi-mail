import React from 'react';
import {useHistory} from 'react-router-dom';

const EmailCard = ({data, isActive}) => {

  const history = useHistory();

  //*********** ADVANCED *********
  // const redirect = () => {
  //   history.push(`${history.location.pathname}/${data.id}`)
  // };
  //*********** ADVANCED ends *********

  const redirect = () => {
    history.push(`/emails/${data.id}`)
  };

  return (
    <div onClick={() => !isActive && redirect()}>
      <h2>{data.from}</h2>
      <h2>{data.to}</h2>
      <h2>{data.subject}</h2>
      {isActive && <p>{data.text}</p>}
      <hr/>
    </div>
  );
};

export default EmailCard;