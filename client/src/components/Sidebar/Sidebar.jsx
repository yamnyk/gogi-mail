import React from 'react';
import {NavLink} from "react-router-dom";
import './Sidebar.scss'

const Sidebar = ({foldersList}) => {
  const links = foldersList.map((folderName, index) => (
    <NavLink
      key={index}
      to={`/emails/${folderName}`}
      className={'navigation__link'}
      activeClassName={'navigation__link--active'}>
      {folderName}
    </NavLink>
  ));

  return (
    <nav className={'navigation'}>
      {links}
    </nav>
  );
};

export default Sidebar;