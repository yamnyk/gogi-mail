import React, {useState} from 'react';
import EmailCard from "./EmailCard";

const EmailsList = ({list}) => {
  const emails = list.map(email => <EmailCard key={email.id} data={email}/>)
  return (
    <div>
      {emails}
    </div>
  );
};

export default EmailsList;