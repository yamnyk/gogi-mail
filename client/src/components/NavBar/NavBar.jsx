import React from 'react';
import {NavLink} from "react-router-dom";
import "./NavBar.scss"

const NavBar = () => {
  return (
    <ul className='navbar'>
      <li>
        <NavLink exact className={'navbar__link'} activeClassName={'navbar__link--active'} to={'/'}>Home</NavLink>
      </li>
      <li>
        <NavLink exact className={'navbar__link'} activeClassName={'navbar__link--active'} to={'/about'}>About</NavLink>
      </li>
      <li>
        <NavLink exact className={'navbar__link'} activeClassName={'navbar__link--active'} to={'/emails'}>Emails</NavLink>
      </li>
    </ul>
  );
};

export default NavBar;