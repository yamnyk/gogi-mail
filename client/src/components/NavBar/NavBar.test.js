import React from 'react';
import { unmountComponentAtNode, render } from 'react-dom';
import { act } from 'react-dom/test-utils';
import { NavBar } from './NavBar';

let container = null;

beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null
});

// beforeAll
// afterAll

describe('Testing NavBar.js', () => {
  test('smoke test - NavBar.js', () => {
    act(() => {
      render(<NavBar />, container)
    })
    // console.log(container.innerHTML)
  })
});
