import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {shallow} from 'enzyme';
import EmailsList from "./EmailsList";

const _CARDS = Object.freeze([
  {
    id: '01',
    from: 'from',
    to: 'to',
    subject: 'subje',
    text: 'text',
  }
]);

describe("EmailsList tests", () => {
  test('EmailsList smoke test', () => {
    jest.mock('./EmailCard.jsx', () => () => <>card</>);
    screen.debug();
    render(<EmailsList list={_CARDS}/>)
  })
  test('emails list shalow render', () => {
    const wrapper = shallow(<EmailsList list={_CARDS}/>);
    screen.debug();
    expect(wrapper.find('div')).toBeInTheDocument()
  })
});