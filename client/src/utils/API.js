const API = {
  getEmails: async () => {
    const resp = await fetch('email.json');
    return await resp.json()
  }
};

export default API;