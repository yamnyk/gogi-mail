import React from 'react';
import {Route, Switch} from "react-router-dom";
import EmailsList from "../components/EmailsList";
import EmailPage from "../pages/EmailPage";

const EmailsRoutes = ({collection}) => {
  return (
    <Switch>
      {/*<Route path={'/emails/:emailId'}*/}
      {/*  render={(rProps) => <EmailPage__advanced {...rProps} list={collection}/>}/>*/}
      <Route exact path={'/emails/inbox'}>
        <EmailsList list={collection.inbox}/>
      </Route>
      <Route exact path={'/emails/sent'}>
        <EmailsList list={collection.sent}/>
      </Route>
      <Route exact path={'/emails/draft'}>
        <EmailsList list={collection.draft}/>
      </Route>
      <Route exact path={'/emails/spam'}>
        <EmailsList list={collection.spam}/>
      </Route>
      <Route path={'/emails/:emailId'}
             render={(rProps) => <EmailPage {...rProps} list={collection}/>}/>
      {/*<Route path={'/emails/:collectionName/:id'}*/}
      {/*       render={() => <EmailPage__hook list={collection}/>}/>*/}
    </Switch>
  );
};

export default EmailsRoutes;