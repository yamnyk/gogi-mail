import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import Home from "../pages/Home";
import Emails from "../pages/Emails";
import About from "../pages/About";
import Login from "../pages/Login";
import PrivateRoute from "./PrivateRoute";
import {useSelector} from "react-redux";

const AppRoutes = () => {
  const isAuth = Boolean(useSelector(state => state.auth.token));

  return (
    <Switch>
      <Route path={'/login'} render={(rProps) => isAuth ? <Redirect to={'/emails'} /> : <Login {...rProps}/>}/>
      <Route path={'/about'} component={About}/>
      <PrivateRoute path={'/emails'} component={Emails}/>
      <Route path={'/'} component={Home}/>
    </Switch>
  );
};

export default AppRoutes;