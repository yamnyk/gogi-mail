import React from 'react';
import {Route, Redirect} from "react-router-dom";
import {useSelector} from "react-redux";

const PrivateRoute = ({render, component:Component, ...restProps}) => {
  const isAuth = Boolean(useSelector(state => state.auth.token));

  return (
    <Route {...restProps} render={() => {
      if(isAuth) {
        return Boolean(render)
          ? render(restProps)
          : <Component {...restProps}/>
      }
      return <Redirect to={'/login'}/>
    }}/>
  );
};

export default PrivateRoute;