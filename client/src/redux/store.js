import {applyMiddleware, combineReducers, createStore} from "redux";
import emailsReducer from "./emails";
import authReducer from "./auth";
import {composeWithDevTools} from "redux-devtools-extension";
import thunk from "redux-thunk";
import favouritesReducer from "./favourites"

const rootReducer = combineReducers(
  {
    emails: emailsReducer,
    auth: authReducer,
    ...favouritesReducer
  }
);

const store = createStore(rootReducer,
  composeWithDevTools(
    applyMiddleware(thunk)
  )
);

export default store;