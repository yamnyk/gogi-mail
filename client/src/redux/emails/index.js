import emailsReducer from "./reducers.js";

export {default as emailsTypes} from './types';
export {default as emailsActions} from './actions';

export default emailsReducer