import {emailsTypes} from "./index";
const initialEmails = {inbox: [], sent: [], draft: [], spam: []};


export default (state = initialEmails, action) => {
  switch (action.type) {
    case emailsTypes.EMAILS_SET:
      return action.payload
    case emailsTypes.EMAILS_ERROR:
      throw new Error("Something wrong with EMAILS actions")
    default:
      return state
  }
}