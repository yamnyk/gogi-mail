import {emailsTypes} from "./index";

const getAllEmailsAction = () => ({
  type: emailsTypes.EMAILS_GET
});

const setAllEmailsAction = (payload) => ({
  type: emailsTypes.EMAILS_SET,
  payload
});

const setAllEmails = () => (dispatch, getState) => {
  fetch('/api/emails', {
    headers: {
      'Content-Type': 'application/json',
      'authorization': getState().auth
    }
  })
    .then(res => res.json())
    .then(data => {
      dispatch(setAllEmailsAction(data))
      // dispatch({
      //   type: emailsTypes.EMAILS_SET,
      //   ...data
      // })
    });
};

export default {
  getAllEmailsAction,
  setAllEmails
}