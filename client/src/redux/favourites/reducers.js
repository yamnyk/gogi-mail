import types from "./types";

const reducer = (state =[], action) => {
  const {type, payload} = action;
  const {ADD_TO_FAVOURITES, REMOVE_FROM_FAVOURITES, GET_FAVOURITES} = types;

  if (type === GET_FAVOURITES) {
    return payload
  } else if ( type === ADD_TO_FAVOURITES) {
    return [...state, payload]
  } else if (type === REMOVE_FROM_FAVOURITES) {
    const stateCopy = [...state];
    const indexOfID = state.indexOf(payload);
    return stateCopy.splice(indexOfID, 1)
  } else {
    return state
  }
};

const reducerFavourites = {
  favourites: reducer
};

export default reducerFavourites