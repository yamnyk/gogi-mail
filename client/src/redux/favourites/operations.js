import actions from "./actions";

const getFavourites = (token) => (dispatch) => {
  fetch("/api/favs", {
    headers: {
      "Content-Type": "application/json",
      "authorization" : token
    }
  })
    .then(response => response.json())
    .then(data => dispatch(actions.getFavourites(data)))
    .catch(err => console.error(err.message))
};

const addToFavourites = (id) => ({dispatch, getState}) => {
  const token = getState().auth;
  fetch("/api/favs", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "authorization" : token
    },
    body: JSON.stringify({id: id})
  })
    .then(response => response.json())
    .then(data => console.log(data))
    .catch(err => console.error(err))
    dispatch(actions.addToFavourites(id))
}


const removeFromFavourites = (id) => ({dispatch, getState}) => {
  const token = getState().auth;
  fetch(`/api/favs/${id}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      "authorization": token
    }
  })
    .then(response => response.json())
    .catch(err => console.error(err.message))
}

export default {
  addToFavourites,
  getFavourites,
  removeFromFavourites
}