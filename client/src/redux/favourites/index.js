import reducer from "./reducers";

export {default as favouritesTypes} from './types';
export {default as favouritesActions} from './actions';
export {default as favouritesSelectors} from './selectors';

export default reducer;