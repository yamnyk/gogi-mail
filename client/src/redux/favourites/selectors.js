const favourites = state => state.favourites;

const selectorsFavourites = {
  favourites
};

export default  selectorsFavourites