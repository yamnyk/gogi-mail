import types from "./types";

const getFavourites = (data) => ({
  type: types.GET_FAVOURITES,
  payload: data
});

const addToFavourites = (id) => ({
  type: types.ADD_TO_FAVOURITES,
  payload: id
});

const removeFromFavourites = (id) => ({
  type: types.REMOVE_FROM_FAVOURITES,
  payload: id
});

const actionsFavourites =  {
  getFavourites,
  addToFavourites,
  removeFromFavourites
};

export default actionsFavourites;