import authReducer from "./reducers";

const initialAuth = {
  token: null,
  error: null
};

describe("auth reducer tests", () => {
  test('whether reducer works', () => {
    const newAuthState = authReducer(undefined, {type: "@@INIT"});

    expect(newAuthState).toStrictEqual(initialAuth);
  });

  test('auth reducer with invalid action type', () => {
    const state = {token: null, error: 'wrong credentials'};
    const newAuthState = authReducer(state, {type: "gogi"});

    expect(newAuthState).toStrictEqual(state);
  })
});