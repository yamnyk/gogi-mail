const AUTH = "AUTH";
const AUTH_ERROR = "AUTH_ERROR";

export default {
  AUTH,
  AUTH_ERROR
};
