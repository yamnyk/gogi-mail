import types from "./types";

const initState = {
  token: null,
  error: null
};

const authReducer = (state = initState, action) => {
  switch (action.type) {
    case types.AUTH:
      return {
        ...state,
        token: action.payload
      };
    case types.AUTH_ERROR:
      return {
        ...state,
        error: action.payload
      };
    default:
      return state;
  }
};

export default authReducer;