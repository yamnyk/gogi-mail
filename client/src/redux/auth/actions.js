import types from './types';

const authAction = (newToken) => ({
  type: types.AUTH,
  payload: newToken
});
const authError = (msg) => ({
  type: types.AUTH_ERROR,
  payload: msg
});

const getToken = (userName) => (dispatch) => {
  fetch('/api/auth', {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({name: userName})
  })
    .then(r => r.json())
    .then(res => {
      if (res.status >= 200 && res.status < 300) {
        dispatch(authAction(res.token))
      } else {
        dispatch(authError(res.message));
      }
    })
    .catch(e => {
      dispatch(authError(e.message));
      console.error(e)
    })
};

export default {authAction, getToken};
