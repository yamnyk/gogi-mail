import React from 'react';
import { unmountComponentAtNode, render } from 'react-dom';
import { act } from 'react-dom/test-utils';
import About from "./About";

let container = null;

beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null
});

// beforeAll
// afterAll

describe('Testing About.js', () => {
  test('smoke test - About.js', () => {
    act(() => {
      render(<About />, container)
    })
    // console.log(container.innerHTML)
  })
});
