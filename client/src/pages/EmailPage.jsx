import React from 'react';
import EmailCard from "../components/EmailCard";

const EmailPage = ({match, list}) => {
  const emailId = match.params.emailId;
  const oneEmail = Object.values(list).flat(1).find((email) => email.id === emailId);
  
  console.log(oneEmail);

  return (
    <div>
      Hello, Gogi!

      <EmailCard isActive={true} data={oneEmail}/>
    </div>
  );
};

export default EmailPage;