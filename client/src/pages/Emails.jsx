import React, {useEffect, useState} from 'react';
import Sidebar from "../components/Sidebar/Sidebar";
import {Redirect, Route, Switch} from "react-router-dom";
import EmailsRoutes from "../routes/EmailsRoutes";
import {useDispatch, useSelector} from "react-redux"
import {emailsActions} from "../redux/emails";


const Emails = () => {
  const dispatch = useDispatch();
  const allEmails = useSelector(state => state.emails);

  useEffect(() => {
    dispatch(emailsActions.setAllEmails())
  }, [emailsActions]);

  if (!allEmails) {
    return <p>loading...</p>
  }

  return (
    <div>
      <h1>Emails</h1>

      <Sidebar foldersList={Object.keys(allEmails)}/>

      <Redirect to={'/emails/inbox'}/>

      <EmailsRoutes collection={allEmails}/>

      {/*<EmailsList list={}/>*/}
    </div>
  );
};
export default Emails;