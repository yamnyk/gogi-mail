import React from "react";
import {shallow} from 'enzyme';
import configureStore from 'redux-mock-store'
import types from "../redux/auth/types";
import Login from "./Login";

const middlewares = [];
const mockStore = configureStore(middlewares);

const authAction = () => ({ type: types.AUTH });

describe('login tests', () => {
  test('login smoke test', () => {
    const initialState = {};
    const store = mockStore(initialState);

    shallow(<Login/>);

    const actions = store.getActions();
    const expectedPayload = { type: types.AUTH };

    expect(actions).toEqual([expectedPayload])
  });
});