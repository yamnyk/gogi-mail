import React from 'react';
import {useRouteMatch} from 'react-router-dom';

const EmailPage = ({list}) => {
  const match = useRouteMatch();
  const emailId = match.params.id;
  const oneEmail = Object.values(list).flat(1).find((email) => email.id === emailId);

  console.log(oneEmail);

  return (
    <div>
      Hello, Gogi!
    </div>
  );
};

export default EmailPage;