import React from 'react';
import {useRouteMatch} from 'react-router-dom';

const EmailPage__advanced = ({list}) => {
  const match = useRouteMatch();
  console.log(match);
  const emailId = match.params.id;
  const collectionName = match.params.collectionName;
  const oneEmail = list[collectionName].find((email) => email.id === emailId);

  console.log(oneEmail);

  return (
    <div>
      Hello, Gogi!
    </div>
  );
};

export default EmailPage__advanced;