import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import About from "./About";

describe('Testing About.js', () => {
  test('smoke test - About.js', () => {
    const {container} = render(<About/>);
    const {getByTestId} = screen;

    const allHeaders = getByTestId('aboutHead');

    expect(allHeaders).toHaveTextContent('About');

    screen.debug();
    // console.log(container.innerHTML)
  });

  // test('counter increases after click', () => {
  //   const mockFunc = jest.fn();
  //   const {getByTestId} = screen;
  //   render(<About counter={0} handleCount={new mockFunc()}/>);
  //
  //   const btn = getByTestId('incCount');
  //   fireEvent(btn, new MouseEvent('click'));
  //
  //   expect(mockFunc).toBeCalled();
  // })

  test('counter increases after click', () => {
    const mockFunc = jest.fn();
    const {getByTestId} = screen;
    render(<About counter={0} handleCount={mockFunc}/>);
    const btn = getByTestId('incCount');
    userEvent.click(btn);
    expect(mockFunc).toBeCalled();
  })
});
