import React, {useRef} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {authActions} from "../redux/auth";

const Login = ({history}) => {
  const dispatch= useDispatch();
  const token = useSelector(state => state.auth.token);
  const authError = useSelector(state => state.auth.error);
  const nameRef = useRef();
  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(authActions.getToken(nameRef.current.value));
  };

  return (
    <form onSubmit={handleSubmit}>
      <input ref={nameRef} type="text" placeholder={'enter your name'}/>
      {authError && <span style={{color:'red'}}>{authError}</span>}
      <input type="submit" value={'log in'}/>
    </form>
  );
};

export default Login;