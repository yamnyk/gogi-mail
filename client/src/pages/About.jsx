import React, {useState} from 'react';

/**
 *
 * @param counter {Number} - initial counter value
 * @param handleCount - click handler for counter btn
 * @return {ReactNode}
 * @constructor
 */
const About = ({counter, handleCount}) => {
  return (
    <div>
      <h1 data-testid="aboutHead" title="don't click me">About</h1>
      <h1>{counter}</h1>
      <button data-testid='incCount' onClick={handleCount}>+</button>
    </div>
  );
};

export default About;